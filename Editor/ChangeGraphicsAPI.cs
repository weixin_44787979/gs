using UnityEngine;
using UnityEditor;

public class ChangeGraphicsAPIEditor
{
    // 添加一个菜单项，用于执行图形 API 设置
    [MenuItem("Tools/Change Graphics API to DX12 & Switch Platform")]
    public static void ChangeToDX12AndSwitchPlatform()
    {
        // 获取当前的图形 API 列表
        UnityEngine.Rendering.GraphicsDeviceType[] graphicsAPIs = PlayerSettings.GetGraphicsAPIs(BuildTarget.StandaloneWindows);

        // 查找 DX12 是否存在
        int dx12Index = System.Array.IndexOf(graphicsAPIs, UnityEngine.Rendering.GraphicsDeviceType.Direct3D12);

        // 如果不存在 DX12，则将其添加
        if (dx12Index == -1)
        {
            // 扩展图形 API 数组
            UnityEngine.Rendering.GraphicsDeviceType[] newGraphicsAPIs = new UnityEngine.Rendering.GraphicsDeviceType[graphicsAPIs.Length + 1];
            newGraphicsAPIs[0] = UnityEngine.Rendering.GraphicsDeviceType.Direct3D12;

            // 将现有的图形 API 按顺序添加到新数组中
            for (int i = 0; i < graphicsAPIs.Length; i++)
            {
                newGraphicsAPIs[i + 1] = graphicsAPIs[i];
            }

            // 设置新的图形 API 列表
            PlayerSettings.SetGraphicsAPIs(BuildTarget.StandaloneWindows, newGraphicsAPIs);
            Debug.Log("Direct3D12 was added and set as the first graphics API.");
        }
        else
        {
            // 如果 Direct3D12 已经存在，则将其移到首位
            UnityEngine.Rendering.GraphicsDeviceType[] newGraphicsAPIs = new UnityEngine.Rendering.GraphicsDeviceType[graphicsAPIs.Length];
            newGraphicsAPIs[0] = UnityEngine.Rendering.GraphicsDeviceType.Direct3D12;

            // 将其他图形 API 按原顺序添加到新列表中（不包括 Direct3D12）
            int j = 1;
            for (int i = 0; i < graphicsAPIs.Length; i++)
            {
                if (i != dx12Index)
                {
                    newGraphicsAPIs[j++] = graphicsAPIs[i];
                }
            }

            // 设置新的图形 API 列表
            PlayerSettings.SetGraphicsAPIs(BuildTarget.StandaloneWindows, newGraphicsAPIs);
            Debug.Log("Direct3D12 was moved to the first position.");
        }

        // 禁用 Auto Graphics API
        PlayerSettings.SetUseDefaultGraphicsAPIs(BuildTarget.StandaloneWindows, false);
        Debug.Log("Auto Graphics API has been disabled.");

        // 自动切换平台为 Windows Standalone
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);
        Debug.Log("Platform switched to Standalone Windows.");

        // 弹出提示框，询问是否重启项目
        bool restart = EditorUtility.DisplayDialog(
            "Restart Required",
            "The platform and Graphics API have been changed. Do you want to restart the project to apply the changes?",
            "Yes",  // 按钮名称
            "No"    // 按钮名称
        );

        if (restart)
        {
            // 如果用户选择了 "Yes"，则重启项目
            RestartProject();
        }
    }

    // 重启当前项目
    private static void RestartProject()
    {
        // 获取当前项目的路径
        string projectPath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/Assets"));

        // 重启项目
        EditorApplication.OpenProject(projectPath);

        Debug.Log("Project will restart to apply the changes.");
    }
}
